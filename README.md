# info-string

Arduino Pro Micro firmware to work with LED matrix display 32x8 leds build on MAX7219 chip. 

Connect display pins DATA, CLK, and CS to 9, 5 and 7 pins of Arduino Pro Micro. Flash firmware. Send text to Serial port in Win-1251 encoding. 

First byte describes how text will be displayed:

* 0x00 - Text is scrolled from right to left
* 0x01 - Text is aligned to the left side
* 0x02 - Text is aligned to the right side
* 0x03 - Text is aligned to the middle
* 0x04 - Display is inversed
* 0x05 - Display is switched back to normal mode
* 0x06 - Clear display

Cyrillic font is supported. 